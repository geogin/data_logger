set(CMAKE_SYSTEM_NAME Generic)

set(AVR_TOOLCHAIN_DIR "" CACHE STRING "specify path to the toolchain
directory")
if(AVR_TOOLCHAIN_DIR)
  set(PATH_PREFIX "${AVR_TOOLCHAIN_DIR}/bin/")
endif()

set(CMAKE_C_COMPILER ${PATH_PREFIX}avr-gcc)
set(CMAKE_CXX_COMPILER ${PATH_PREFIX}avr-g++)

#set(CMAKE_FIND_ROOT_PATH /home/bvb/Projects/avr8-gnu-toolchain-linux_x86_64/)
set(CMAKE_FIND_ROOT_PATH ${AVR_TOOLCHAIN_DIR})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
