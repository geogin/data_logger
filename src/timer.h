/*
 * timer.h
 *
 *  Created on: Aug 10, 2017
 *      Author: bvb
 */

#ifndef SRC_INCLUDE_TIMER_H_
#define SRC_INCLUDE_TIMER_H_

#include <avr/io.h>

typedef void (*Callback)(void*);

void timer_start(Callback callback, void* arg);
void timer_stop();



#endif /* SRC_INCLUDE_TIMER_H_ */
