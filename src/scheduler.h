
#ifndef SRC_SCHEDULER_H_
#define SRC_SCHEDULER_H_

#include "stdint.h"

typedef void (*Task)(void);
#define MAX_TASKS 10

/*
 * @brief: Initialize scheduler.
 *
 * Method to be invoked before using api's provided by this module. The
 * scheduler runs at a tick rate of 1 tick per ms.
 * The scheduler can accommodate at-most MAX_TASKS.
 *
 * */
void scheduler_init(void);

/*
 * @brief: Start the scheduler loop.
 **/
void scheduler_run(void);

/*
 * @brief: Add a task to the scheduler
 *
 * The scheduler will execute the provided task once the delay specified
 * expires.
 *
 * @param t       : The user specified task to be executed. The task is expected
 *                  to be a function pointer.
 *
 * @param delay_ms: Specifies the the time the scheduler waits before executing
 *                  the provided task. Can be 0;
 *
 * @return a uint8_t indicating success. 1 is returned when the task has been
 * added successfully and 0 on failure.
 *
 **/
uint8_t scheduler_add(Task t, uint16_t delay_ms);

/*
 * @brief: Adds a periodic task to the scheduler.
 *
 * @param t        : The user task that should be executed periodically.
 *
 * @param period_ms: The task will be executed every period_ms. 0 is not a valid
 *                   value for this parameter.
 *
 * @return uint8_t indicating success; 1 indicates success, 0 indicates failure.
 * Calling this method with period_ms = 0 would result in returning 0.
 **/
uint8_t scheduler_addPeriodic(Task t, uint16_t period_ms);

/*
 * @brief: removes indicated task from the scheduler queue
 *
 **/
void scheduler_remove(Task t);

#endif /* SRC_SCHEDULER_H_ */
