/*
 * testApp.cpp
 *
 * Created: 8/9/2017 10:00:21 PM
 * Author : geo
 */



#include "timer.h"
#include "scheduler.h"

#include <stddef.h>
#include <stdint.h>
#include <interrupt.h>

#define TOGGLE_TIME_IN_MS 1000
#define LED_PORT PORTB
#define LED_PIN PORTB5

/**address of data direction register of port x
 *
 * example: DDRG == DDR(PORTG)
 */
#define DDR(x) (*(&(x)-1))
#define PIN(x) (*(&(x)-2))

void toggleLed();

int main() {
  DDR(LED_PORT) |= (1 << LED_PIN);
  LED_PORT |= (1 << LED_PIN);

  scheduler_init();
  scheduler_addPeriodic(toggleLed, 1000);
  scheduler_run();

}

void toggleLed() {
    PIN(LED_PORT) |= (1 << LED_PIN);
}
