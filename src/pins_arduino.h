/*
 * pins_arduino.h
 *
 *  Created on: Nov 7, 2017
 *      Author: varghese
 */

#ifndef SRC_PINS_ARDUINO_H_
#define SRC_PINS_ARDUINO_H_

#include <avr/pgmspace.h>
/*
 * This file describes the hardware pin mappings of the datalogger
 * board, needed by the arduino libraries
 *
 *
 */

#define INPUT_PULLUP 0x2
#define NOT_A_PIN 0
#define NOT_A_PORT 0

#define PA 1
#define PB 2
#define PC 3
#define PD 4
#define PE 5
#define PF 6
#define PG 7
#define PH 8
#define PJ 10
#define PK 11
#define PL 12

// The 28 pin version has 3 ports:
// - PORTB
// - PORTC
// - PORTD

#define PIN_SPI_SS (0)   // PB2
#define PIN_SPI_MOSI (1) // PB3
#define PIN_SPI_MISO (2) // PB4
#define PIN_SPI_SCK (3)  // PB5

#define PIN_WIRE_SDA (4) // PC4
#define PIN_WIRE_SCL (5) // PC5

static const uint8_t SDA = PIN_WIRE_SDA;
static const uint8_t SCL = PIN_WIRE_SCL;

static const uint8_t SS = PIN_SPI_SS;
static const uint8_t MOSI = PIN_SPI_MOSI;
static const uint8_t MISO = PIN_SPI_MISO;
static const uint8_t SCK = PIN_SPI_SCK;

const uint8_t PROGMEM digital_pin_to_port_PGM[] = {
  PB, // PB2 - SPI_SS
  PB, // PB3 - SPI_MOSI
  PB, // PB4 - SPI_MISO
  PB, // PB5 - SPI_SCK
  PC, // PC4 - TWI_SDA
  PC, // PC5 - TWI_SCL
};

const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[] = {
  _BV(2), // PB2 - SPI_SS
  _BV(3), // PB3 - SPI_MOSI
  _BV(4), // PB4 - SPI_MISO
  _BV(5), // PB5 - SPI_SCK
  _BV(4), // PC4 - TWI_SDA
  _BV(5), // PC5 - TWI_SCL
};

const uint16_t PROGMEM port_to_mode_PGM[] = {
  NOT_A_PORT, NOT_A_PORT, (uint16_t)&DDRB, (uint16_t)&DDRC, (uint16_t)&DDRD,
};

const uint16_t PROGMEM port_to_output_PGM[] = {
  NOT_A_PORT, NOT_A_PORT, (uint16_t)&PORTB, (uint16_t)&PORTC, (uint16_t)&PORTD,
};

const uint16_t PROGMEM port_to_input_PGM[] = {
  NOT_A_PORT, NOT_A_PORT, (uint16_t)&PINB, (uint16_t)&PINC, (uint16_t)&PIND,
};

#endif /* SRC_PINS_ARDUINO_H_ */
