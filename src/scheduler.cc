/*
 * scheduler.c

 *
 *  Created on: Feb 27, 2017
 *      Author: varghese
 */

#include "timer.h"
#include "scheduler.h"

#include <stddef.h>
#include <interrupt.h>

#define TASK_LIST_SIZE \
  (MAX_TASKS + 1)  // The one extra slot is a reserver for re-adding periodic
// tasks. If the task list is full, then we would be forced to
// drop the periodic task; reason being we intentionally do
// not re-add this task to the same task slot. Doing this
// should safe guard against the periodic task hogging up the
// cpu (and starving other tasks) in the case this task is at
// the top of the tasklist and has a period which is shorter
// than the delays of all other tasks below it.

#define PRESCALER_VALUE TC_CLKSEL_DIV256_gc

#define TIMER_PERIOD 125  // Translates to 1 ms at
                          // clock freq of 32Mhz and
                          // prescaler of 256

#define TASK_ADD_FAILURE 0
#define TASK_ADD_SUCCESS 1

typedef struct {
  Task task;
  uint16_t delay_ticks;
  uint16_t period_ticks;
} TaskSlot;

static volatile TaskSlot taskList[TASK_LIST_SIZE];
static uint8_t slotsFree = TASK_LIST_SIZE;

static void clearTaskList(void);
static void initializeTimerModule(void);
static void decrementTaskListTick(void*);
static uint8_t findFreeTaskSlot(void);

/*****************************************************************************/
void scheduler_init() {
  clearTaskList();
  initializeTimerModule();
}

/*****************************************************************************/
void scheduler_run() {
  uint8_t readyTask;

  while (1) {
    // scan for expired  tasks.
    for (readyTask = 0; readyTask < TASK_LIST_SIZE; ++readyTask) {
      //  Not disabling interrupts on purpose; timer ISR does not modify
      //  taskList[i].delay_ticks which are already at 0;

      if ((taskList[readyTask].task != NULL) &&
          (taskList[readyTask].delay_ticks == 0)) {
        break;
      }  // A task is ready to run
    }

    if (readyTask == TASK_LIST_SIZE) {
      continue;
    }  // none of the tasks are ready to run yet.

    taskList[readyTask].task();  // run the ready task

    uint8_t isReadyTaskPeriodic = (taskList[readyTask].period_ticks != 0);
    uint8_t slot = findFreeTaskSlot();
    uint8_t isFreeSlotAvailable = (slot != TASK_LIST_SIZE);

    if (isReadyTaskPeriodic && isFreeSlotAvailable) {
      // critical section: re-add the periodic task
      cli();
      taskList[slot].task = taskList[readyTask].task;
      taskList[slot].delay_ticks = taskList[readyTask].period_ticks;
      taskList[slot].period_ticks = taskList[readyTask].period_ticks;
      sei();
      slotsFree--;
    }  //  We drop the periodic task when no slots are available to relocate
       //  this
    //  periodic task. If we use its current slot, there is a risk
    //  that this task can starve the others below it.

    // remove executed task.
    cli();
    taskList[readyTask].task = NULL;
    taskList[readyTask].delay_ticks = 0;
    taskList[readyTask].period_ticks = 0;
    sei();
    slotsFree++;
  }
}

/*****************************************************************************/
uint8_t scheduler_add(Task t, uint16_t delay_ms) {
  if (t == NULL) {
    return TASK_ADD_FAILURE;
  }

  if (slotsFree <= 1) {  // we need one free slot for periodic tasks to work
    return TASK_ADD_FAILURE;
  }

  uint8_t slot =
      findFreeTaskSlot();  // Above check makes sure we have a free slot.

  // critical section: modify task list
  cli();
  taskList[slot].task = t;
  taskList[slot].delay_ticks = delay_ms;
  taskList[slot].period_ticks = 0;
  sei();
  slotsFree--;

  return TASK_ADD_SUCCESS;
}

/*****************************************************************************/
uint8_t scheduler_addPeriodic(Task t, uint16_t period_ms) {
  if (t == NULL) {
    return TASK_ADD_FAILURE;
  }
  if (slotsFree <= 1) {  // we need one free slot for periodic tasks to work
    return TASK_ADD_FAILURE;
  }

  uint8_t slot =
      findFreeTaskSlot();  // Above check makes sure we have a free slot.

  // critical section: modify task list
  cli();
  taskList[slot].task = t;
  taskList[slot].delay_ticks = 0;  // execute this task right away
  taskList[slot].period_ticks = period_ms;
  sei();
  slotsFree--;

  return TASK_ADD_SUCCESS;
}

/*****************************************************************************/
void scheduler_remove(Task t) {
  if (t == NULL) {
    return;
  }
  for (uint8_t i = 0; i < TASK_LIST_SIZE; ++i) {
    if (taskList[i].task == t) {
      cli();
      taskList[i].task = NULL;
      taskList[i].delay_ticks = 0;
      taskList[i].period_ticks = 0;
      sei();
      slotsFree++;
    }
  }
}

/*****************************************************************************/
static void clearTaskList() {
  for (uint8_t i = 0; i < TASK_LIST_SIZE; ++i) {
    // not clearing interupt on purpose; should nt be required here
    taskList[i].task = NULL;
    taskList[i].delay_ticks = 0;
    taskList[i].period_ticks = 0;
  }
  slotsFree = TASK_LIST_SIZE;
}

/*****************************************************************************/
static void initializeTimerModule() { timer_start(decrementTaskListTick, NULL); }

/*****************************************************************************/
static void decrementTaskListTick(void* unused) {
  for (uint8_t i = 0; i < TASK_LIST_SIZE; ++i) {
    if (taskList[i].delay_ticks != 0) {
      --taskList[i].delay_ticks;
    }
  }
}

/*****************************************************************************/
static uint8_t findFreeTaskSlot() {
  uint8_t slot;
  for (slot = 0; slot < TASK_LIST_SIZE; ++slot) {
    if (taskList[slot].task == NULL) {  // found an empty slot
      break;
    }
  }
  return slot;
}
