/*
 * timer.cc
 *
 *  Created on: Aug 10, 2017
 *      Author: bvb
 */

#include "timer.h"
#include <interrupt.h>

#define TIMER2_INT_vect TIMER2_COMPA_vect
#define ENABLE_CTC_MODE (1 << WGM21)
#define SET_PRESCALER_128 0x05
#define SET_TIMER_INTERRUPT (1 << OCIE2A)
#define CLEAR_TIMER_INTERRUPT_FLAG (1 << OCF2A)
#define TIMER2_CYC_FOR_1MILLISEC 125
#define TIMER_OFF 0xF8

static Callback callback;
static void* arg;

void timer_start(Callback callback_, void* arg_) {
  arg = arg_;
  callback = callback_;
  TCCR2A |= ENABLE_CTC_MODE;
  TCCR2B |= SET_PRESCALER_128;
  // set interrupt mask for OCRA
  TIMSK2 |= SET_TIMER_INTERRUPT;
  // make sure the interrupt flag is cleared
  TIFR2 |= CLEAR_TIMER_INTERRUPT_FLAG;
  // set the comapre value
  OCR2A = TIMER2_CYC_FOR_1MILLISEC;
  // OCR1A -> top value
}

void timer_stop() { TCCR2B &= TIMER_OFF; }

ISR(TIMER2_COMPA_vect) {
  if (callback) {
	  callback(arg);
  }
}
