/*-----------------------------------------------------------------------*/
/* MMCv3/SDv1/SDv2 Controls via AVR SPI module                           */
/*-----------------------------------------------------------------------*/
/*
/  Copyright (C) 2016, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   any purpose as you like UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/-------------------------------------------------------------------------*/

#include "SPI.h"

#include <avr/io.h>

static uint8_t getIndex(uint16_t freqInKhz);
static void setPrescaleValue(uint8_t prescaler);

//  SPI2X SPR01 SPR00
//  0     0     0  Fosc/4 0x00
//  0     0     1  Fosc/16 0x01
//  0     1     0  Fosc/64 0x02
//  0     1     1  Fosc/128 0x03
//  1     0     0  Fosc/2 0x04
//  1     0     1  Fosc/8 0x05
//  1     1     0  Fosc/32 0x06
//  1     1     1  Fosc/64 0x07
constexpr uint16_t freqList[] = {
    F_OSC_KHz / 128, F_OSC_KHz / 64, F_OSC_KHz / 32, F_OSC_KHz / 16,
    F_OSC_KHz / 8,   F_OSC_KHz / 4,  F_OSC_KHz / 2};
constexpr uint8_t numFreq = sizeof(freqList) / sizeof(uint16_t);
constexpr uint8_t prescalerValues[] = {0x03, 0x02, 0x06, 0x01, 0x05, 0x00, 0x04};

void power_on (void)
{
  /* Trun socket power on and wait for 10ms+ (nothing to do if no power
   * controls) */
  // TODO:To be filled

  /* Configure MOSI/MISO/SCLK/CS pins */
  // TODO:To be filled

  /* Enable SPI module in SPI mode 0 */
  // TODO:To be filled
}

void power_off(void) {
  /* Disable SPI function */
  // TODO: To be filled

  /* De-configure MOSI/MISO/SCLK/CS pins (set hi-z) */
  // TODO: To be filled

  /* Trun socket power off (nothing to do if no power controls) */
  // TODO: To be filled
}

/*-----------------------------------------------------------------------*/
/* Transmit/Receive data from/to MMC via SPI  (Platform dependent)       */
/*-----------------------------------------------------------------------*/

/* Exchange a byte */
uint8_t xchg_spi(	    /* Returns received data */
		 uint8_t dat /* Data to be sent */
		 ) {
  SPDR = dat;
  loop_until_bit_is_set(SPSR, SPIF);
  return SPDR;
}

/* Receive a data block fast */

void rcvr_spi_multi(uint8_t *p,  /* Data read buffer */
		    uint32_t cnt /* Size of data block */
		    ) {
  do {
    SPDR = 0xFF;
    loop_until_bit_is_set(SPSR, SPIF);
    *p++ = SPDR;
    SPDR = 0xFF;
    loop_until_bit_is_set(SPSR, SPIF);
    *p++ = SPDR;
  } while (cnt -= 2);
}

/* Send a data block fast */
void xmit_spi_multi(const uint8_t *p, /* Data block to be sent */
		    uint32_t cnt      /* Size of data block */
		    ) {
  do {
    SPDR = *p++;
    loop_until_bit_is_set(SPSR, SPIF);
    SPDR = *p++;
    loop_until_bit_is_set(SPSR, SPIF);
  } while (cnt -= 2);
}

//
// TODO: Testing
// Does spi bus have to be disabled and re enabled?
//
// SPI control register0: SPCR0 + SPSR0
// bits:
// SPR01, SPR00 -> SPCR0
// SPI2X -> SPSR0
//
//  SPI2X SPR01 SPR00
//  0     0     0  Fosc/4
//  0     0     1  Fosc/16
//  0     1     0  Fosc/64
//  0     1     1  Fosc/128
//  1     0     0  Fosc/2
//  1     0     1  Fosc/8
//  1     1     0  Fosc/32
//  1     1     1  Fosc/64
uint16_t change_bus_speed(uint16_t freqInKhz) {
  uint8_t index = getIndex(freqInKhz);
  uint8_t prescaler = prescalerValues[index];
  setPrescaleValue(prescaler);
  return freqList[index];
}


uint16_t enable_spi_module(uint8_t freqInKhz) {
  // What register to use:
  //
  // Things to be done in this register.
}

/*****************************************************************************/
uint8_t getIndex(uint16_t freqInKhz) {
  if (freqInKhz > freqList[numFreq - 1]) {
    return (numFreq - 1);
  }
  uint8_t low = 0;
  uint8_t high = numFreq - 1;
  uint8_t mid = low + (high - low) / 2;

  while (low != mid) {
    if (freqList[mid] == freqInKhz) {
      return mid;
    } else if (freqInKhz > freqList[mid]) {
      low = mid;
    } else {
      high = mid;
    }
    mid = low + (high - low) / 2;
  }
  if (freqList[high] == freqInKhz) {
    return high;
  }
  return low;
}

/*****************************************************************************/
void setPrescaleValue(uint8_t prescaler) {
  //  SPI2X SPR01 SPR00
  //  0     0     0  Fosc/4 0x00
  //  0     0     1  Fosc/16 0x01
  //  0     1     0  Fosc/64 0x02
  //  0     1     1  Fosc/128 0x03
  //  1     0     0  Fosc/2 0x04
  //  1     0     1  Fosc/8 0x05
  //  1     1     0  Fosc/32 0x06
  //  1     1     1  Fosc/64 0x07
  // SPI control register0: SPCR0 + SPSR0
  // bits:
  // SPR01, SPR00 -> SPCR0
  // SPI2X -> SPSR0
  uint8_t tmp1 = SPCR;
  uint8_t tmp2 = SPSR;
  uint8_t spcrContent = prescaler & 0x03;
  uint8_t spsrContent = prescaler >> 2;

  tmp1 &= 0xFC;
  tmp2 &= 0xFE;

  tmp1 |= spcrContent;
  tmp2 |= spsrContent;

  SPCR = tmp1;
  SPSR = tmp2;
}
