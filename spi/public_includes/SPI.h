/*
 * Copyright (c) 2010 by Cristian Maglie <c.maglie@arduino.cc>
 * Copyright (c) 2014 by Paul Stoffregen <paul@pjrc.com> (Transaction API)
 * Copyright (c) 2014 by Matthijs Kooijman <matthijs@stdin.nl> (SPISettings AVR)
 * Copyright (c) 2014 by Andrew J. Kroll <xxxajk@gmail.com> (atomicity fixes)
 * SPI Master library for arduino.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 */

#ifndef _SPI_H_INCLUDED
#define _SPI_H_INCLUDED

#include <stdint.h>



#define F_OSC_KHz F_CPU/1000
//
// CLK SPEED
// RELATION BETWEEN CLK SPEED AND SPI BUS SPEED
// PRESCALER FOR SPI BUS SPPED
//
// #define setClkSpeed()

/*
 * can be used for changing the SPI bus speed. Takes the required
 * frequency in Khz, returns what was set in KHz.
 */
uint16_t change_bus_speed(uint16_t freqInKhz);

uint16_t enable_spi_module(uint8_t freqInKhz);
void disable_spi_module();


uint8_t xchg_spi(            /* Returns received data */
                 uint8_t dat /* Data to be sent */
                 );
void rcvr_spi_multi(uint8_t *p,  /* Data read buffer */
                    uint32_t cnt /* Size of data block */
                    );
void xmit_spi_multi(const uint8_t *p, /* Data block to be sent */
                    uint32_t cnt      /* Size of data block */
                    );

#endif
